# React Js Locale Setter
In this test, you are expected to write a small web application which has the following usability options: 
1. Fill up simple a QnA form and have the options to change the languages in the form. 
2. The type of question, questions and possible answers for these 10 steps are available in questions.json

Your task will be as follows: 
1. Create a stepper form inspired by typeform
2. Allow the user to select the language of questions and answers in the first step. 
3. Create a simple NodeJS + Express server where you import the questions in the desired language from MongoDB collection and return it as a response to a GET command. For Example: GET localhost:3000/questions?locale=EN should return questions in english assuming that your nodejs server is being served at port 3000
4. Call the backend endpoint in your app and display the questions in the form 
5. Create an endpoint in your backend to POST the answers and save the responses to the mongodb collection. 
6. Use the endpoint from 5. in your frontend. 

Knowledge tested: 
1. Understanding of NodeJS + ExpressJS
2. Understanding of REST API 
3. Understanding of MongoDB
4. Understanding of ReactJS

Must haves for the submission: 
1. Ability to use MongoDB and ReactJS

Nice to haves: 
2. Ability to use REST API served over NodeJS

Ports to use: 
1. 5000 for your frontend
2. 3000 for your backend 

Note: If you are unable to use NodeJS, you can directly call mongodb collections via your frontend. 

